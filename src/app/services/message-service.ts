import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {

    public messageListArray: Array<string> = new Array;

    public currentTimeArray: Array<number> = new Array;

    public addMessageToArrays(message: string): void {
        this.messageListArray.push(message);
        this.currentTimeArray.push(Date.now());
    }

    public deleteMessageFromArrays(messageIndex: number): void {
        this.messageListArray.splice(messageIndex, 1);
        this.currentTimeArray.splice(messageIndex, 1);
    }
}
