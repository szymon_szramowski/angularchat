import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MessageService } from '../../services/message-service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {

  @ViewChild('inpucik') private inputValue: ElementRef;

  constructor(public messageService: MessageService) { }

  ngOnInit() {
  }

  public sendMessage(inputText: string): void {
    if (this.inputValue.nativeElement.value !== '') {
      this.messageService.addMessageToArrays(inputText);
      this.inputValue.nativeElement.value = '';
    }
  }

}
